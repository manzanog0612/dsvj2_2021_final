﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSDV_Final.Gameplay.Objects.TankRelated
{
    public class Track : MonoBehaviour
    {
        [SerializeField] private float scrollSpeed = 0.05f;

        private float offset = 0.0f;
        private Renderer r;

        public void ScrollTrack(float velocity)
        {
            if (velocity == 0) return;

            if (velocity < 0)
            {
                offset = (offset + Time.deltaTime * scrollSpeed) % 1f;
            }
            else
            {
                offset = (offset - Time.deltaTime * scrollSpeed) % 1f;
            }

            r.material.SetTextureOffset("_MainTex", new Vector2(offset, 0f));
        }

        void Start()
        {
            r = GetComponent<Renderer>();
        }
    }
}
