﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSDV_Final.Gameplay.Interfaces
{
    public interface IHittable
    {
        void ReceiveDamage();
    }
}

