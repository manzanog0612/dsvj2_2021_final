﻿using System;
using UnityEngine;

using TSDV_Final.Gameplay.User;
using TSDV_Final.Gameplay.Objects.TankRelated.Bullets;
using TSDV_Final.Gameplay.Targets.Static;
using TSDV_Final.Gameplay.Targets;
using TSDV_Final.Gameplay.Targets.Dynamic;
using TSDV_Final.Management;

namespace TSDV_Final.Gameplay.Management
{
    public class GameplayManager : MonoBehaviour
    {
        private static GameplayManager instance = null;
        public static GameplayManager Instance { get => instance;}

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
            }            
        }

        [SerializeField] private Player player = null;
        [SerializeField] private Target[] targets = null;
        [SerializeField] private Enemy[] enemies = null;
        [SerializeField] private BulletInstantiator[] bulletInstantiators = null;
        [SerializeField] private Box[] boxes = null;
        [SerializeField] private Bomb[] bombs = null;

        public Action<int> OnPointsChanged = null;

        private float gameTimeSeconds = 120f;
        private bool gamePaused = false;

        private int points = 0;
        private int destroyedBoxes = 0;
        private int killedEnemies = 0;

        public Player Player { get => player; }
        public float GameTimeSeconds { get => gameTimeSeconds; }
        public int Points { get => points; }
        public int DestroyedBoxes { get => destroyedBoxes; set => destroyedBoxes = value; }
        public int KilledEnemies { get => killedEnemies; set => killedEnemies = value; }
        
        private void UpdateGameTime()
        {
            gameTimeSeconds -= Time.deltaTime;

            if (GameTimeSeconds <= 0f)
            {
                GameManager.Instance.GoToScene(GameManager.Scene.ResultScreen);
            }
        }

        public void SetPause(bool pause)
        {
            Time.timeScale = pause ? 0 : 1;
            gamePaused = pause;
        }

        private void AddScore()
        {
            points += 5;

            OnPointsChanged?.Invoke(points);
        }

        private void AddDestroyedBox()
        {
            destroyedBoxes++;
        }

        private void AddKilledEnemy()
        {
            killedEnemies++;
        }

        private void DamagePlayer()
        {
            player.ReceiveDamage();
        }

        private void EndGame()
        {
            GameManager.Instance.GoToScene(GameManager.Scene.ResultScreen);
        }

        private void AssingBombActions(Bomb bomb)
        {
            bomb.OnExplode += DamagePlayer;
        }

        private void Start()
        {
            player.OnDie += EndGame;

            for (int i = 0; i < targets.Length; i++)
            {
                targets[i].OnDie += AddScore;
            }

            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].OnDie += AddDestroyedBox;
            }

            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].OnDie += AddKilledEnemy;
                bulletInstantiators[i].OnCreatedBomb += AssingBombActions;
            }

            for (int i = 0; i < bombs.Length; i++)
            {
                bombs[i].OnExplode += DamagePlayer;
            }
        }

        private void Update()
        {
            if (gamePaused)
            { 
                return; 
            }
            else
            {
                UpdateGameTime();
            }
        }
    }
}
