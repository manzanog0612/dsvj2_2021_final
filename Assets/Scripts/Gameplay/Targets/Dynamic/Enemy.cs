﻿using System.Collections;
using UnityEngine;

using TSDV_Final.Gameplay.Objects.TankRelated;

namespace TSDV_Final.Gameplay.Targets.Dynamic
{
    public class Enemy : Target
    {
        [SerializeField] private Transform target = null;
        [SerializeField] private Tank tank = null;

        private const float initialTimeToShoot = 5;
        private float timeToShoot = 5;

        private IEnumerator dieInst = null;

        private void TankUpdate()
        {
            timeToShoot -= Time.deltaTime;

            if (timeToShoot < 0)
            {
                Vector3 turretDir = target.position - tank.Turret.transform.position;
                tank.Turret.SetTurretMovement(turretDir);
                timeToShoot = initialTimeToShoot;
            }

            Vector3 tankDir = transform.position + (target.position - transform.position).normalized;

            tank.SetRotation(tankDir);

            tank.SetMovement(1f);
        }

        private void Start()
        {
            Durability = Durability.Weak;
        }

        private void Update()
        {
            if (HitsTaken == (int)Durability)
            {
                if (dieInst == null)
                {
                    dieInst = tank.Rotate(2);
                    StartCoroutine(dieInst);
                }
                return;
            }

            TankUpdate();
        }
    }
}
