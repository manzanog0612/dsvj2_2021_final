﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSDV_Final.Gameplay.Targets.Static
{
    public class Bomb : Target
    {
        public Action OnExplode;
        public Action<Bomb> OnStart;

        private bool hit = false;

        private void Start()
        {
            Durability = Durability.Weak;
            OnStart?.Invoke(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player" && !hit)
            {
                ReceiveDamage();
                OnExplode?.Invoke();
                hit = true;
            }
        }
    }
}
