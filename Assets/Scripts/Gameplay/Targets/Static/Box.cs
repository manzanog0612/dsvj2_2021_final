﻿using UnityEngine;

namespace TSDV_Final.Gameplay.Targets.Static
{
    public class Box : Target
    {
        private void Start()
        {
            int amountDurabilities = 3;

            Durability = (Durability)UnityEngine.Random.Range((int)Durability.Weak, amountDurabilities + 1);
            // + 1 because durability enum starts in 1, not 0
            
            Color color;

            switch (Durability)
            {
                case Durability.Weak:
                    color = Color.green;
                    break;
                case Durability.Normal:
                    color = Color.yellow;
                    break;
                case Durability.Strong:
                    color = Color.red;
                    break;
                default:
                    color = Color.yellow;
                    break;
            }

            Mesh.material.color = color;

            DeathEffect.GetComponent<Renderer>().material.color = color;
        }
    }
}

