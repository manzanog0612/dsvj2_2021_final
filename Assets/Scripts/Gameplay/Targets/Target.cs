﻿using System;
using UnityEngine;

using TSDV_Final.Gameplay.Interfaces;

namespace TSDV_Final.Gameplay.Targets
{
    public enum Durability { Weak = 1, Normal, Strong }

    public class Target : MonoBehaviour, IHittable
    {
        [SerializeField] private ParticleSystem deathEffect = null;

        private Durability durability = Durability.Normal;

        private int hitsTaken = 0;

        public Action OnHitted = null;
        public Action OnDie = null;

        private Animator animator = null;
        private MeshRenderer mesh = null;

        public ParticleSystem DeathEffect { get => deathEffect; set => deathEffect = value; }
        public Durability Durability { get => durability; set => durability = value; }
        public Animator Animator { get => animator; set => animator = value; }
        public MeshRenderer Mesh { get => mesh; set => mesh = value; }
        public int HitsTaken { get => hitsTaken; set => hitsTaken = value; }

        private void Awake()
        {
            mesh = GetComponent<MeshRenderer>();
            animator = GetComponent<Animator>();
            animator.enabled = false;
        }

        public void ReceiveDamage()
        {
            hitsTaken++;

            OnHitted?.Invoke();

            if (hitsTaken == (int)durability)
            {
                animator.enabled = true;
                if (deathEffect != null)
                { 
                    deathEffect.Play(); 
                }
                OnDie?.Invoke();
            }
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }
    }
}


