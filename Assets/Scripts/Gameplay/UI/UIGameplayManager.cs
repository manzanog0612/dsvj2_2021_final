﻿using System;
using UnityEngine;
using UnityEngine.UI;

using TSDV_Final.Gameplay.Management;
using TSDV_Final.Management;

namespace TSDV_Final.Gameplay.UI
{
    public class UIGameplayManager : MonoBehaviour
    {
        [SerializeField] private Text timer = null;
        [SerializeField] private Text points = null;
        [SerializeField] private Text distance = null;
        [SerializeField] private GameObject pauseButton = null;
        [SerializeField] private GameObject continueButton = null;
        [SerializeField] private GameObject pausePanel = null;

        private GameplayManager gameplayManager = null;

        private void UpdatePoints(int _points)
        {
            if (_points < 100)
            {
                points.text = "0";

                if (_points < 10)
                {
                    points.text += "0";
                }
            }

            points.text += _points.ToString();
        }

        private void UpdateDistance(float _distance)
        {
            if (_distance < 100)
            {
                int dis = (int)_distance;

                distance.text = "00.";

                if (dis < 10 )
                {
                    distance.text += "0";
                }

                distance.text += dis.ToString();
            }
            else
            {
                int num1 = (int)_distance / 1000;
                int num2 = (int)(_distance - num1 * 1000) / 100;
                int num3 = (int)(_distance - num1 * 1000 - num2 * 100) / 10;
                int num4 = (int)(_distance - num1 * 1000 - num2 * 100 - num3 * 10);

                distance.text = num1.ToString() + num2.ToString() + "." + num3.ToString() + num4.ToString();
            }

            distance.text += "km";
        }

        private string FormatTime(float fTime)
        {
            TimeSpan t = TimeSpan.FromSeconds(fTime);

            return string.Format("{1:D2}:{2:D2}:{3:D2}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
        }

        public void ShowPausePanel(bool show)
        {
            pausePanel.SetActive(show);
            continueButton.SetActive(show);
            pauseButton.SetActive(!show);
        }
        public void GoToMenu()
        {
            GameManager.Instance.GoToScene(GameManager.Scene.Menu);
            Time.timeScale = 1;
        }

        private void Start()
        {
            gameplayManager = GameplayManager.Instance;

            gameplayManager.OnPointsChanged += UpdatePoints;
            gameplayManager.Player.OnMoved += UpdateDistance;            
        }

        private void Update()
        {
            timer.text = FormatTime(gameplayManager.GameTimeSeconds);
        }

        private void OnDestroy()
        {

        }
    }
}

