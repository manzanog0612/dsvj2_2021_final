﻿using System;
using System.Collections;
using UnityEngine;

namespace TSDV_Final.Gameplay.Objects.TankRelated
{
    public class Tank : MonoBehaviour
    {
        [SerializeField] private float movementSpeed = 5f;
        [SerializeField] private float rotationSpeed = 3f;
        [SerializeField] private Track[] track = null;
        [SerializeField] private Turret turret = null;

        public Action<float> OnMoved;

        [SerializeField] private int hits = 5;

        public int Hits { get => hits; set => hits = value; }
        public Turret Turret { get => turret; }
        public float MovementSpeed { get => movementSpeed; }
        public float RotationSpeed { get => rotationSpeed; }

        public IEnumerator Rotate(float duration)
        {
            float amountTurns = 2;

            float startRotation = transform.eulerAngles.y;
            float endRotation = startRotation + 360f * amountTurns;

            float t = 0.0f;

            while (t < duration)
            {
                t += Time.deltaTime;

                float yRotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360f * amountTurns;

                transform.eulerAngles = new Vector3(transform.eulerAngles.x, yRotation, transform.eulerAngles.z);

                yield return new WaitForEndOfFrame();
            }

            yield return null;
        }

        private void MoveTracks(float velocity)
        {
            for (int i = 0; i < track.Length; i++)
            {
                track[i].ScrollTrack(velocity);
            }
        }

        private Vector3 ProjectPointOnPlane(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {
            planeNormal.Normalize();
            var distance = -Vector3.Dot(planeNormal, (point - planePoint));
            return point + planeNormal * distance;
        }

        public void SetRotation(Vector2 direction)
        {
            RaycastHit hit;

            Physics.Raycast(transform.position, -Vector3.up, out hit);

            //Setting tank upper rotation
            //{
            Quaternion newRotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, rotationSpeed * Time.deltaTime);
            //}

            //Setting tank target position
            //{
            Vector3 target = transform.position + (transform.forward * Mathf.Abs(direction.y) + transform.right * direction.x) * Time.deltaTime * movementSpeed;
            //}

            //Setting tank side rotation
            //{
            Vector3 targetPos = ProjectPointOnPlane(transform.up, transform.position, target);
            newRotation = Quaternion.LookRotation(targetPos - transform.position, transform.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, rotationSpeed * Time.deltaTime);
            //}
        }

        public void SetRotation(Vector3 targetDir)
        {
            RaycastHit hit;

            Physics.Raycast(transform.position, -Vector3.up, out hit);

            //Setting tank upper rotation
            //{
            Quaternion newRotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, rotationSpeed * Time.deltaTime);
            //}

            //Setting tank side rotation
            //{
            Vector3 targetPos = ProjectPointOnPlane(transform.up, transform.position, targetDir);
            newRotation = Quaternion.LookRotation(targetPos - transform.position, transform.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, rotationSpeed * Time.deltaTime);
            //}
        }

        public void SetMovement(float velocity)
        {
            transform.position = transform.position + transform.forward * velocity * Time.deltaTime * movementSpeed;

            MoveTracks(velocity);
        }
    }
}

