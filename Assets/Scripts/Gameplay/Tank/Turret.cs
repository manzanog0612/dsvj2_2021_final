﻿using System.Collections;
using UnityEngine;

using TSDV_Final.Gameplay.Objects.TankRelated.Bullets;

namespace TSDV_Final.Gameplay.Objects.TankRelated
{
    public class Turret : MonoBehaviour
    {
        [SerializeField] private LayerMask hitLayer = 0;
        [SerializeField] private float rotationSpeed = 3f;
        [SerializeField] private BulletInstantiator bulletInstantiator = null;
        [SerializeField] private ParticleSystem shotEffect = null;

        private IEnumerator rotateInst = null;

        public LayerMask HitLayer { get => hitLayer; }
        public BulletInstantiator BulletInstantiator { get => bulletInstantiator;}

        private IEnumerator Rotate(Quaternion finalRotation, Vector3 dirVector)
        {
            Quaternion initialRotation = transform.rotation;
            float t = 0f;

            while (t < 1f)
            {
                t += Time.deltaTime * rotationSpeed;

                transform.rotation = Quaternion.Lerp(initialRotation, finalRotation, t);

                yield return new WaitForEndOfFrame();
            } 

            bulletInstantiator.InstantiateBullet(dirVector);
            shotEffect.Play();

            yield return null;
        }

        public void SetTurretMovement(Vector3 dirVector)
        {
            //float minMagnitude = 10;
            //float maxXValue = 3;

            Quaternion newRotation = Quaternion.LookRotation(dirVector, transform.up);

            //Vector3 bulletDir = hitPoint - bulletInstantiator.EndOfCanyon.transform.position;
            //if (dirVector.magnitude < minMagnitude)
            //{
            //    Vector3 myRotation = newRotation.eulerAngles;
            //    myRotation.x = Mathf.Clamp(myRotation.x, -1000, maxXValue);
            //    newRotation = Quaternion.Euler(myRotation);
            //}

            if (rotateInst != null)
            {
                StopCoroutine(rotateInst);
            }

            rotateInst = Rotate(newRotation, dirVector);
            StartCoroutine(rotateInst);
        }
    }
}
