﻿using System;
using UnityEngine;

using TSDV_Final.Gameplay.Interfaces;

namespace TSDV_Final.Gameplay.Objects.TankRelated.Bullets
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private LayerMask layerMask;

        private void OnCollisionEnter(Collision collision)
        {
            IHittable hittable = collision.gameObject.GetComponent<IHittable>();

            if (hittable != null)
            {
                hittable.ReceiveDamage();
            }

            Destroy(gameObject);
        }
    }
}
