﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TSDV_Final.Gameplay.Targets.Static;

namespace TSDV_Final.Gameplay.Objects.TankRelated.Bullets
{
    public class BulletInstantiator : MonoBehaviour
    {
        [SerializeField] private GameObject bulletPrefab = null;
        [SerializeField] private bool isBomb = false;

        [SerializeField] private GameObject endOfCanyon = null;
        [SerializeField] private float bulletSpeed = 5f;

        public Action<Bomb> OnCreatedBomb = null;

        public GameObject EndOfCanyon { get => endOfCanyon; }

        private void CreateBomb(Bomb bomb)
        {
            OnCreatedBomb?.Invoke(bomb);
        }
        
        public void InstantiateBullet(Vector3 dirVector)
        {
            Vector3 startPos = endOfCanyon.transform.position + endOfCanyon.transform.forward * bulletPrefab.transform.lossyScale.y;

            Quaternion initialRotation = Quaternion.LookRotation(endOfCanyon.transform.forward, endOfCanyon.transform.up) * Quaternion.Euler(90,0,0);

            GameObject bullet = Instantiate(bulletPrefab, startPos, initialRotation);

            bullet.GetComponent<Rigidbody>().AddForce(dirVector.normalized * bulletSpeed * 50);

            if (isBomb)
            {
                bullet.GetComponent<Bomb>().OnStart += CreateBomb;
            }
        }
    }
}
