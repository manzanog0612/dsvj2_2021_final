﻿using System;
using UnityEngine;

using TSDV_Final.Gameplay.Objects.TankRelated;
using TSDV_Final.Gameplay.Interfaces;

namespace TSDV_Final.Gameplay.User
{
    public class Player : Tank, IHittable
    {
        private Vector3 input = Vector3.zero;

        public Action OnDie = null;

        private Ray ray;
        private RaycastHit hitData;

        private float traveledDistance = 0f;
        private bool lockedTurret = false;

        private Animator animator = null;

        public float TraveledDistance { get => traveledDistance; set => traveledDistance = value; }
        public bool LockedTurret { get => lockedTurret; set => lockedTurret = value; }

        private void Awake()
        {
            animator = GetComponent<Animator>();
            animator.enabled = false;
        }

        public void Die()
        {
            OnDie?.Invoke();
        }

        public void ReceiveDamage()
        {
            Hits--;

            if (Hits == 0)
            {
                StartCoroutine(Rotate(2));
                animator.enabled = true;
            }
        }

        public void GetMousePositionInWorld()
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Physics.Raycast(ray, out hitData, 5000, Turret.HitLayer, QueryTriggerInteraction.Ignore);
        }

        private bool IsMoving()
        {
            input.x = Input.GetAxis("Horizontal");
            input.y = Input.GetAxis("Vertical");

            return Mathf.Abs(input.x) > 0f || Mathf.Abs(input.y) > 0f;
        }

        void Update()
        {
            if (Hits == 0)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (lockedTurret) return;

                GetMousePositionInWorld();
                Vector3 dirVector = hitData.point - Turret.transform.position;
                Turret.SetTurretMovement(dirVector);
            }

            if (IsMoving())
            {
                SetRotation((Vector2)input);
                SetMovement(input.y);
                traveledDistance += Mathf.Abs(input.y * Time.deltaTime * MovementSpeed);
                OnMoved?.Invoke(traveledDistance);
            }
        }
    }
}

