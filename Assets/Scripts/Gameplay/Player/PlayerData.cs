﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSDV_Final.Gameplay.Objects
{
    public class PlayerData
    {
        public PlayerData(int _points, int _destroyedBoxes, float _traveledDitance, int _killedEnemies, bool _playerDead)
        {
            points = _points;
            destroyedBoxes = _destroyedBoxes;
            traveledDitance = _traveledDitance;
            killedEnemies = _killedEnemies;
            playerDead = _playerDead;
        }

        private int points = 0;
        private int destroyedBoxes = 0;
        private float traveledDitance = 0f;
        private int killedEnemies = 0;
        private bool playerDead = false;

        public int Points { get => points; }
        public int DestroyedBoxes { get => destroyedBoxes; }
        public float TraveledDitance { get => traveledDitance; }
        public int KilledEnemies { get => killedEnemies; }
        public bool PlayerDead { get => playerDead; }

        public void UpdateData(int _points, int _destroyedBoxes, float _traveledDitance, int _killedEnemies, bool _playerDead)
        {
            points = _points;
            destroyedBoxes = _destroyedBoxes;
            traveledDitance = _traveledDitance;
            killedEnemies = _killedEnemies;
            playerDead = _playerDead;
        }
    }
}
