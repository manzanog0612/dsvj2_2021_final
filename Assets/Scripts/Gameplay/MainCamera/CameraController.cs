﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSDV_Final.Gameplay.MainCamera
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private GameObject target;
        [SerializeField] private Vector3 positionFromTraget = Vector3.zero;
        [SerializeField] private Vector3 inclination = Vector3.zero;

        private void Start()
        {
            inclination.x = 30f;
        }

        private void Update()
        {
            transform.position = target.transform.position + positionFromTraget;
            transform.eulerAngles = inclination;
        }
    }
}

