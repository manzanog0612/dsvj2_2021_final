﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TSDV_Final.Management;

namespace TSDV_Final.Menu.UI
{
    public class UIMenuMananger : MonoBehaviour
    {
        [SerializeField] private GameObject menu = null;
        [SerializeField] private GameObject credits = null;

        public void GoToScene(int scene)
        {
            GameManager.Instance.GoToScene((GameManager.Scene)scene);
        }

        public void SwitchToCredits()
        {
            menu.SetActive(false);
            credits.SetActive(true);
        }

        public void SwitchToMenu()
        {
            menu.SetActive(true);
            credits.SetActive(false);
        }

        public void ExitGame()
        {
            GameManager.Instance.CloseGame();
        }
    }
}
