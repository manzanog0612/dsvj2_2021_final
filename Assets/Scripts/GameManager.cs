﻿using UnityEngine;
using UnityEngine.SceneManagement;

using TSDV_Final.Gameplay.User;
using TSDV_Final.Gameplay.Management;
using TSDV_Final.Gameplay.Objects;

namespace TSDV_Final.Management
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager instance = null;
        public static GameManager Instance { get => instance;}

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        public enum Scene { Menu, Game, ResultScreen }

        private PlayerData playerData = null;
        public PlayerData PlayerData { get => playerData; set => playerData = value; }

        public void GoToScene(Scene scene)
        {
            string stringSceneName;

            switch (scene)
            {
                case Scene.Menu:
                    stringSceneName = "Menu";
                    break;
                case Scene.Game:
                    stringSceneName = "Game";
                    break;
                case Scene.ResultScreen:
                    stringSceneName = "ResultScreen";
                    break;
                default:
                    stringSceneName = "Menu";
                    break;
            }

            SceneManager.LoadScene(stringSceneName);

            if (scene == Scene.ResultScreen)
            {
                SetPlayerData();
            }
        }

        public void CloseGame()
        {
            Application.Quit();
        }

        public void SetPlayerData()
        {
            GameplayManager gameplayManager = GameplayManager.Instance;
            Player tank = gameplayManager.Player;

            int points = gameplayManager.Points;
            int destroyedBoxes = gameplayManager.DestroyedBoxes;
            float traveledDistance = tank.TraveledDistance;
            int killedEnemies = gameplayManager.KilledEnemies;
            bool playerDead = tank.Hits == 0;

            playerData = new PlayerData(points, destroyedBoxes, traveledDistance, killedEnemies, playerDead);
        } 
    }
}